
We implemented the ID3 Algorithm in Java so if you have a prefered way to open and run java files then go ahead with the normal way you do things.

I should mention before you go that there is a part of the code that asks for the directory of the dataset you're using. I found the formatting is rather particular.
For me the directory is /Users/Me/A3/dm_a3/data/ID3Data.csv
Make sure to use the Tree and Node classes

If you don't have a normal way to run java files then I suggest going to terminal/command line/command prompt 
Then navigate to wherever you downloaded the repository by typing in cd and the directory you want to go to next and clicking enter
Then go to /src/ID3 and click enter
From here type in "java Main" without the quotes and click enter
After that you need to tell the program where the data is at
For me the directory is /Users/Me/A3/dm_a3/data/ID3Data.csv
Don't forget about .csv after you type in the directory and the slash at the start.
If you get it wrong and the program crashes then just reopen the java file by doing "java Main"
You'll also need to put in the pruning limit.
The pruning limit is the value after this many nodes is left in a example the tree returns a node with how likely it thinks a patient will go to the appointment
The program should take roughly 2-5 seconds to run depending on how fast your computer is
It outputs a text version of the tree 

From here you can copy that code and put it in a text file 

In the future it would be ideal if we could run a translation algorithm that allows values to be taken from the 
text file and makes them into a 'Node' object.
The visualization code runs in Processing, and each 'Node' object needs a name, a generation number(starting at 0), 
the number of nodes that are in that generation, the node number (AKA, out of the number of nodes in the generation,
which one is this?), and the path that was taken to arrive at that node.
Each object has a make() and a makeLine(child) method. While slightly too much work to be feasible to use on large
trees, given more time it would be possible to make something to do most of this work for you.

For our second algorithm we chose CART.

To run this algorithm, you need to make sure the dataset "PreProcessedDataCartNoNeighbourhood.csv" is in the same directory as "CART.py". 
You will also need to import and install graphViz and SKLearn into your python enviornment. This can be done through pip, conda, or any other method of choice. This command differs based on your python version, but can usually be done through the command line.

python -m pip install graphviz
python -m conda install graphviz

You may need to replace 'python' with 'py' or the version depending on your python enviornment and OS.

Then, simply run "CART.py". 
A visualization will be automatically generated in "CARTOut.pdf".  